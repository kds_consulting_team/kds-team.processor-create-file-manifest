import logging
import shutil
from pathlib import Path

from keboola.component.base import ComponentBase
from keboola.component import UserException
# configuration variables
from keboola.component.dao import FileDefinition

from tag_functions import TagFunctions
from tag_functions.tagfunctions import FunctionValidationException

KEY_ENCRYPTED = 'encrypted'
KEY_TAGS = 'tags'
KEY_IS_PERMANENT = 'is_permanent'
KEY_IS_PUBLIC = 'is_public'
KEY_NOTIFY = 'notify'
KEY_TAG_FUNCTIONS = 'tag_functions'

# list of mandatory parameters => if some is missing,
# component will fail with readable message on initialization.
REQUIRED_PARAMETERS = [KEY_TAGS]
REQUIRED_IMAGE_PARS = []


class Component(ComponentBase):
    """
        Extends base class for general Python components. Initializes the CommonInterface
        and performs configuration validation.

        For easier debugging the data folder is picked up by default from `../data` path,
        relative to working directory.

        If `debug` parameter is present in the `config.json`, the default logger is set to verbose DEBUG mode.
    """

    def __init__(self):
        super().__init__(required_parameters=REQUIRED_PARAMETERS,
                         required_image_parameters=REQUIRED_IMAGE_PARS)

        if not isinstance(self.configuration.parameters[KEY_TAGS], list):
            raise UserException('Invalid type! The tags parameter has to be an array of strings!')

    def run(self):
        '''
        Main execution code
        '''

        in_files = self.get_input_files_definitions(only_latest_files=False)
        for file in in_files:
            logging.info(f'Updating manifest for file {file.full_path}')
            self.move_and_update_manifest(file)

        if self.configuration.parameters.get('keep_tables', False):
            logging.info("Keeping out tables intact")
            self.move_tables()

        logging.info('Finished.')

    def move_tables(self):
        shutil.copytree(self.tables_in_path, self.tables_out_path, dirs_exist_ok=True)

    def move_and_update_manifest(self, in_file: FileDefinition):
        params = self.configuration.parameters

        file_name = Path(in_file.full_path).name
        new_out_file = self.create_out_file_definition(file_name)

        new_out_file.tags = self.build_file_tags(in_file)
        new_out_file.is_permanent = params.get(KEY_IS_PERMANENT, in_file.is_permanent)
        new_out_file.notify = params.get(KEY_NOTIFY, in_file.notify)
        new_out_file.is_encrypted = params.get(KEY_ENCRYPTED, in_file.is_encrypted)
        new_out_file.is_public = params.get(KEY_IS_PUBLIC, in_file.is_public)
        self.move_file_to_out(in_file.full_path, new_out_file)

    def move_file_to_out(self, sourc_path, file: FileDefinition):
        # move in_file to out
        shutil.copy(sourc_path, file.full_path)
        self.write_manifest(file)

    def build_file_tags(self, in_file):
        params = self.configuration.parameters
        file_name = Path(in_file.full_path).name
        tags = params[KEY_TAGS]

        tags.extend(self.get_tags_from_functions(file_name=file_name))
        return tags

    def get_tags_from_functions(self, file_name):
        new_tags = []
        try:
            for f in self.configuration.parameters.get(KEY_TAG_FUNCTIONS, []):
                new_tags.extend(TagFunctions.perform_function(f, file_name))
        except FunctionValidationException as e:
            raise UserException(e) from e

        return new_tags


"""
        Main entrypoint
"""
if __name__ == "__main__":
    try:
        comp = Component()
        comp.run()
    except UserException as exc:
        logging.exception(exc)
        exit(1)
    except Exception as exc:
        logging.exception(exc)
        exit(2)
