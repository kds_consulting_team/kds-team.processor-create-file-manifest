import logging

import re
from pathlib import Path
from typing import Optional, List


def perform_custom_function(self, key, function_cfg, user_params):
    if function_cfg.get('attr'):
        return user_params[function_cfg['attr']]
    if not function_cfg.get('function'):
        raise ValueError(
            F'The user parameter {key} value is object and is not a valid function object: {function_cfg}')
    new_args = []
    for arg in function_cfg.get('args'):
        if isinstance(arg, dict):
            arg = self._perform_custom_function(key, arg, user_params)
        new_args.append(arg)
    function_cfg['args'] = new_args

    return self.user_functions.__execute_single_function(function_cfg['function'], *function_cfg.get('args'))


def _get_position_in_parts(parts, position, function_name=''):
    if position is not None:
        if len(parts) <= position:
            logging.warning(
                f"Invalid position index {position} in function {function_name}. The split parts are: {parts}")
            parts = []
        else:
            parts = [parts[position]]
    return parts


class FunctionValidationException(Exception):
    pass


class TagFunctions:
    """
    Custom function to be used in configruation
    """

    @classmethod
    def perform_function(cls, function_cfg, file_name, user_params=None) -> List[str]:
        """
        triggers function chain
        """
        cls.validate_function_config(function_cfg)
        if not user_params:
            user_params = {}

        if function_cfg.get('attr'):
            return user_params[function_cfg['attr']]

        positional_args = []
        for arg in function_cfg.get('args', []):
            if isinstance(arg, dict):
                arg = cls.perform_function(arg, file_name, user_params)
            positional_args.append(arg)
        function_cfg['args'] = positional_args

        named_args = {}
        optional_args = function_cfg.get('optional_args', {})
        # pop_tag prefix
        tag_prefix = optional_args.pop('tag_prefix', '')
        for arg_key in optional_args:
            value = optional_args[arg_key]
            if isinstance(value, dict):
                value = cls.perform_function(optional_args[arg_key], file_name, user_params)
            named_args[arg_key] = value

        function_cfg['optional_args'] = named_args

        tags = cls.__execute_single_function(function_cfg['function'], file_name, function_cfg.get('optional_args', {}),
                                             *function_cfg.get('args'))
        # tag_prefix
        tags = cls.__add_tag_prefix(tags, tag_prefix)
        return tags

    @staticmethod
    def validate_function_config(function_config: dict):
        errors = []
        if not function_config.get('function'):
            errors.append(F'The configuration is not a valid function object: {function_config}')

        if not isinstance(function_config.get('args', []), list):
            errors.append(
                f'args parameter type in function {function_config.get("function")} is invalid, it must be an array!')

        if not isinstance(function_config.get('optional_args', {}), dict):
            errors.append(
                f'optional_args parameter type  in function {function_config.get("function")} is invalid, it must be '
                f'an object!')
        if errors:
            raise FunctionValidationException('; '.join(errors))

    @staticmethod
    def __add_tag_prefix(tags: List[str], tag_prefix: str):
        if tag_prefix:
            tags = [tag_prefix + ': ' + part for part in tags]
        return tags

    @classmethod
    def validate_function_name(cls, function_name):
        supp_functions = cls.get_supported_functions()
        if function_name not in cls.get_supported_functions():
            raise ValueError(
                F"Specified user function [{function_name}] is not supported! "
                F"Supported functions are {supp_functions}")

    @classmethod
    def get_supported_functions(cls):
        return [method_name for method_name in dir(cls)
                if callable(getattr(cls, method_name)) and not method_name.startswith('__')]

    @classmethod
    def __execute_single_function(cls, function_name, file_name, optional_args, *pars):
        cls.validate_function_name(function_name)
        return getattr(TagFunctions, function_name)(file_name, *pars, **optional_args)

    # ########## SUPPORTED FUNCTIONS
    @staticmethod
    def filename_split(file_name: str, separator: str, maxsplit: int = -1, position: Optional[int] = None):
        parts = file_name.split(separator, maxsplit)
        parts = _get_position_in_parts(parts, position, 'filename_split')

        return parts

    @staticmethod
    def regex_groups_match(file_name: str, regex: str, position: Optional[int] = None):
        match = re.match(regex, file_name)
        if not match:
            return []
        parts = match.groups()
        parts = _get_position_in_parts(parts, position, 'regex_groups_match')
        return parts

    @staticmethod
    def file_name(file_name: str, full_path: bool = False):
        if not full_path:
            file_name = Path(file_name).name
        return [file_name]
