'''
Created on 12. 11. 2018

@author: esner
'''
import unittest

from tag_functions import TagFunctions


class TestFunctions(unittest.TestCase):

    def test_split_function_basic(self):
        configuration = {"function": "filename_split",
                         "args": ['_']}
        expected = ['some', 'file', 'split']
        tags = TagFunctions.perform_function(configuration, 'some_file_split')
        self.assertEqual(expected, tags)

    def test_split_function_maxsplit(self):
        configuration = {"function": "filename_split", "args": ['_', 1]}
        expected = ['some', 'file_split']
        tags = TagFunctions.perform_function(configuration, 'some_file_split')
        self.assertEqual(expected, tags)

    def test_split_function_position(self):
        configuration = {"function": "filename_split",
                         "args": ['_'],
                         "optional_args": {"position": 1}
                         }
        expected = ['file']
        tags = TagFunctions.perform_function(configuration, 'some_file_split')
        self.assertEqual(expected, tags)

    def test_regex_groups(self):
        test = "somefile_123.csv"
        configuration = {"function": "regex_groups_match",
                         "args": ['(\\w+)_(\\d+)\\.(\\w+)'],
                         "optional_args": {"position": 1}
                         }
        expected = ['123']
        tags = TagFunctions.perform_function(configuration, test)
        self.assertEqual(expected, tags)


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
